package com.salman.contactdelete;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button button;
    ListView listView;
    TextView texViewEmpty;
    ArrayList<String> arraylistName;
    public ArrayList<String> arrayListID;
    TextView textView;
    CustomAdapter adapter;
    private Uri uriContact;
    final Context context = this;
    static String contactName = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button =(Button)findViewById(R.id.alldeletebutton);

        listView = (ListView) findViewById(R.id.sampleListView);
        texViewEmpty = (TextView)findViewById(R.id.texViwEmpty);

        arraylistName = new ArrayList<>();
        arrayListID = new ArrayList<>();
        textView = (TextView) findViewById(R.id.tv);
        adapter = new CustomAdapter(this, arraylistName);
        listView.setAdapter(adapter);
//        adapter.notifyDataSetChanged();
//        listView.invalidateViews();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0;i<=(arrayListID.size() -1);i++){
                    final String pos = arrayListID.get(i);
                    int delete = getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build(), ContactsContract.RawContacts._ID + " = ?", new String[]{pos});

                }


            }
        });



       adapterSet();

        Log.d("TAG", "Contact Name: " + contactName);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), "this contact will be delete when click to Yes", Toast.LENGTH_SHORT).show();
                final String pos = arrayListID.get(position);


                AlertDialog.Builder alert = new AlertDialog.Builder(
                        MainActivity.this);
                alert.setTitle("are you sure you want to delete");
                alert.setMessage("single delete");
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {


                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("TAG", "YES Button Clicked : " + dialog.toString());
                        Toast.makeText(getApplicationContext(), "Successfully deleted", Toast.LENGTH_SHORT).show();
                        int delete = getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build(), ContactsContract.RawContacts._ID + " = ?", new String[]{pos});
//                        arraylistName.clear();
//                        initListWithData();
//                        listView.invalidateViews();
//                        listView.deferNotifyDataSetChanged();
                        arraylistName.clear();
                        arrayListID.clear();
                        adapterSet();


                    }

                });


                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {


                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d("TAG", "YES Button Clicked : " + dialog.toString());
                        Toast.makeText(getApplicationContext(), "Successfully Cancel", Toast.LENGTH_SHORT).show();
//                        int delete = getContentResolver().delete(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter(ContactsContract.CALLER_IS_SYNCADAPTER, "true").build(), ContactsContract.RawContacts._ID + " = ?", new String[]{pos});
//                        arraylistName.clear();
//                        initListWithData();
//                        listView.invalidateViews();
//                        listView.deferNotifyDataSetChanged();


                    }

                });

                AlertDialog dialog = alert.create();
                dialog.show();


            }


        });


            }




            private void adapterSet() {
                final Uri uriContact = ContactsContract.Contacts.CONTENT_URI;

                // querying contact data store
                Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);


                    cursor.moveToFirst();


                do {
                    contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    int contactID = cursor.getInt(cursor.getColumnIndex(ContactsContract.RawContacts._ID));

                    Log.d("TAGR", contactName + ", ID:" + contactID);

                    if (!contactName.equals(null)){

                        arrayListID.add(String.valueOf(contactID));
                        arraylistName.add(contactName);

                    }

                } while (cursor.moveToNext());

                // DISPLAY_NAME = The display name for the contact.
                // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.
                listView.setAdapter(adapter);
                listView.setEmptyView(texViewEmpty);



                cursor.close();


            }

    }






