package com.salman.contactdelete;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aditya on 1/28/16.
 */


    public class CustomAdapter extends BaseAdapter {

        Activity context = null;

        ArrayList<String> arrayList = null;


        public CustomAdapter(Activity context, ArrayList<String> object) {
            this.context = context;
            this.arrayList = object;
        }

       class ViewHolder {

            public TextView name;

        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            String name = arrayList.get(position);
            ViewHolder viewHolder = null;

            View rowView = convertView;
            if (convertView == null) {

                LayoutInflater inflater = context.getLayoutInflater();
                rowView = inflater.inflate(R.layout.list_item, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.name = (TextView) rowView.findViewById(R.id.tv);

                rowView.setTag(viewHolder);
            } else {



            viewHolder = (ViewHolder) rowView.getTag();

            }
            viewHolder.name.setText(name);

            return rowView;
        }
    }


